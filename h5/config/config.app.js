var GLOBAL_CONFIG = {
	'API_HOST': 'https://ecshoptest.xyunqi.com',
	'APP_NAME': 'Demo',
	'APP_DESC': 'This is a demo',
	'APP_KEYWORDS': 'Demo, Shop',
	'DEBUG': false,
	'ENCRYPTED': false,
	'FOR_WEIXIN': false
};